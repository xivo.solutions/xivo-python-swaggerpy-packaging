# The packaging information for python swagger-py in XiVO

This repository contains the packaging information for
[python-swagger-py](https://github.com/digium/swagger-py).

To get a new version of python-swagger-py in the XiVO repository, set the
desired version in the `VERSION` file and increment the changelog.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.
